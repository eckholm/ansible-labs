# Welcome to Lab3

Now we have gotten to know Ansible a little more. 
We have allready played with: 
- Message of the day
- Ansible Facts and variables
- Roles and Collections
- Continues deployments
- Installing packages
- Deploying a website
- Firewall configurations 
and more

In this lab we will use what we have allready learned in Lab 1 and 2 to deploy databases, more advanced use of variables and system configurations and best practices. 

Lets jump into 1_databases to setup a mysql database, automate the configurations, create som users and more. 


## Requirements for lab:
# Database system deployment

### In this lab we will look at and learn how to:
- Create a DBA user from Ansible Vault encrypted variable file
- Get to know Ansible Set Fact
- More advanved Variables
- Configure firewalls
- Deploy a database server and create a database
- Some git best practices
- Delegation of Ansible tasks

#### Note:
Before starting this lab, it is recommended to take a **snapshot** of the VM if you plan to run the lab multiple times.

**Ansible.builtin.set_fact.** This module allowes us to "set a ansible fact" and use it as a variable later on. <br>
This is mostly used for times you have to type in the same multiple times, but can also be used to run whole scripts and filters +++. 

## Ansible Vault
Ansible Vault provides a way to encrypt and manage sensitive data such as passwords, ssh keys, api keys and more. 


# Tasks:
### 1. First, lets create our config file in the project root direcotry
Since we need sudo priveleges for most of the tasks, we will use the **become** method in the config file for this playbook. <br>
~./ansible.cfg
```
collections = ./collections/
roles = ./roles/
inventory = ./inventory
vault_password_file = ./secret_pass

[privilege_escalation]
become = true
become_method = sudo
become_user = root
become_ask_pass = true

[galaxy]
url=https://galaxy.ansible.com 

```
Then create the roles and collections directories as you did in earlier tasks.

### 2. Now create the requirements.yml file we need to install our collections
~./collections/requirements.yml

```
---
collections:
  - name: community.mysql
    version: 3.9.0

  - name: ansible.posix
    version: 1.5.4

```
Then now, install the collections in the requirements.yml file with the command: <br>
```[user@workstation ~]$ ansible-galaxy collection install -r collections/requirements.yml -p ./collections```

### 3. Lets create our roles by running ansible-galaxy
```[user@workstation ~]$ ansible-galaxy init roles/users``` <br>
```[user@workstation ~]$ ansible-galaxy init roles/firewall``` <br>
```[user@workstation ~]$ ansible-galaxy init roles/database``` <br>

### 4. Create Admin User
#### Ansible Vault
**Ansible Vault** will prompt us for password each time it wants to decrypt the contect of our **secrets.yml** file which is an encrypted file we store users passwords in. We do not want to store this file in a 
public git repository or unencrypted. Therefore we will use ansible-vault to encrypt it and a password file for ansible to read when to encrypt the passwords file. 

For this we need to introduce a new variable into our ansible.cfg file. 
```
[defaults]
collections = ./collections/
roles = ./roles/
inventory = ./inventory
vault_password_file = secret_pass <---
```
Now we dont need to point ansible to the secret_pass file or be prompted for a password when running this playbook with cli later in this lab. <br>
Now lets create the **secret_pass** file with the vault/encryption password in it by running this command: 

```
[user@workstation ~]$ echo ansible-kda > secret_pass
```

#### Create user role
Lets create our ansible user 

~./roles/users/tasks/main.yml
```
---
- name: Create user from secrets
  ansible.builtin.user:
    name: "{{ username }}"
    password: "{{ password }}"
```
Since we want our role to be reusable and not use the same variables for each user we create at a later time, we do not want to add our encrypted variable file in the role directory. <br>
Since we usually store our code in a git repository, we will follow some best practices in this labs. We do not want to secret and company related data in git where it can be viewed by others so to combat this, we will create a 
"git ignore file(.gitignore)". This is a file that tells git to skip data added to this file. <br>
Lets create .gitignore in our project root directory and add our files: 

~./.gitignore
```
./secrets.yml   # <--- This is the user credentials file
./run.yml       # <--- This is our playbook (If you are sharing to airgapped git, this dont need to be added. If you share to a public git, dont share your playbook)
./secret_pass   # <--- The password for our vault encryption (unencrypted file)
```

#### Create the secrets
Run the following command:
```
[user@workstation ~]$ cat > secrets.yml << EOL
username: ansibleuser
password: ansiblepassword
EOL
[user@workstation ~]$ 
```
Note: Best practice is to used hashed passwords, but for simplicity we do not use that now. 

Now lets encrypt the file with the following command: <br>
```[user@workstation ~]$ ansible-vault encrypt secrets.yml``` <br>
If you now cat the file, you will see that the text is encrypted and not usable. 

To see the text inside the file we can use **ansible-vault** to decrypt the content with the command: <br>
```[user@workstation ~]$ ansible-vault view secrets.yml```

### 5. Start on the playbook
For simplicity of this lab, lets start our playbook and fill it in as we go along, so you dont need to remember to much later. 

~./run.yml
```
- name: Deploy a database server with user and data
  hosts: localhost
  connection: local
  gather_facts: true


  roles:
    - role: users
      vars_files:
       - secrets.yml
```

### 6. Configure Firewall
Next we will confire our firewall. <br>
The default port for a mysql server is 3306/tcp. We want applications to be able to use our sql server so we need to open this port but we want to restrict it to our "pretend" application with IP address: 192.168.100.90 <br>

~./roles/firewall/tasks/main.yml
```
---
- name: Configure firewall for mysql
  ansible.posix.firewalld:
    port: "{{ firewall_port }}"
    permanent: "{{ permanent_state }}"
    state: "{{ firewall_state }}"

```
Now when we have created our firewall task. Lets add the role to our playbook. 

~./run.yml
```
- name: Deploy a database server with user and data
  hosts: localhost
  connection: local
  gather_facts: true


  pre_tasks:
    ansible.builtin.set_fact:
      ansible_app_ip: 192.168.100.90
      mysql_default_port: 3306/tcp

  roles:
    - role: users
    - role: firewall
      vars:
        - firewall_ip_address: "{{ ansible_app_ip }}"
        - firewall_port: "{{ mysql_default_port }}"
        - firewall_state: enabled
        - permanent_state: true
```

### 7. Deploy and configure our database server
We want to use mysql as our database server and the users we created earlier as the user with access. We also want to add some data to it, so lets start on that right away. <br>
And for practice, lets add some handlers, role variables and use facts from the playbook. 

~./roles/database/tasks/main.yml
```
---
- name: Install mysql-server
  ansible.builtin.package:
    name: mysql-server
    state: present
    update_cache: true

- name: Install mysql-client and python dependencies
  ansible.builtin.shell: | 
    curl -sSLO https://dev.mysql.com/get/mysql80-community-release-el9-4.noarch.rpm
    rpm -ivh mysql80-community-release-el9-4.noarch.rpm
    yum install -y mysql-devel
    yum install -y xmlsec1  xmlsec1-openssl
    export PATH=/usr/local/bin:$PATH
    pip3 install PyMySQL

- name: start_mysql
  ansible.builtin.systemd_service:
    name: mysqld.service
    state: started
    enabled: true

- name: Create the mysql user
  community.mysql.mysql_user:
    login_user: root
    login_password:
    name: "{{ db_username }}"
    password: "{{ db_password }}"
    priv: "{{ db_priviliges }}"
    state: present

- name: Create a database
  community.mysql.mysql_db:
    name: "{{ db_name }}"
    state: present
  notify: Query


```
~./roles/database/handlers/main.yml
```
---
- name: Query
  ansible.builtin.shell: | 
    mysql -u root -e "SHOW DATABASES;"
  register: query_out
  notify: Print Query

- name: Print Query
  ansible.builtin.debug:
    msg: "{{ query_out.stdout_lines }}"


```
~./roles/database/vars/main.yml
```
---
db_username: "{{ username }}"
db_password: "{{ password }}"

```

Wow, that was a lone one. (and yet, it is quit small)<br>
Lets go over what we did here. 
- We downloaded and install the latest version of mysql-server and mysql-client
- We created a new user with admin role to all databases (dba)
- We enabled remote login to the database to the ip address we ourself diseded in the "Set Facts" module. 
- We created some handlers and some role variables. 
But about thos role variables... Why? What is the point to create a variable and then create another with allmost the same name and add it to our playbook? 

Lets fill in the database role into our playbook and see if you can see it by your self. 

~./run.yml
```
- name: Deploy a database server with user and data
  hosts: localhost
  connection: local
  gather_facts: true


  pre_tasks:
    ansible.builtin.set_fact:
      ansible_app_ip: 192.168.100.90
      mysql_default_port: 3306/tcp

  roles:
    - role: users

    - role: firewall
      vars:
        - firewall_ip_address: "{{ ansible_app_ip }}"
        - firewall_port: "{{ mysql_default_port }}"
        - firewall_state: enabled
        - permanent_state: true

    - role: database
      vars_files::
        - secrets.yml
      vars:
        - db_priviliges: '*.*:ALL'
        - db_name: ansible_app

```

Did you see it? <br>
We reused the vault variables for our db user. The user we created should also be the db user, and we dont want to have multiple variables and secret files for the same user right? We want to reuse what we have. 
While trying to reuse as much as we can it is also important to keep track of it all. This can take some practice, therefor a good documentation is very important.

Remember to document all roles as you do them. 

No, at last lets finnish our playbook with a post_task: to check if everything is running as it supposed to, and for fun....- Lets delegate the task itself to the remote host and run some local shell:

~./run.yml
```
- name: Deploy a database server with user and data
  hosts: localhost
  connection: local
  gather_facts: true


  pre_tasks:
    - name: Setting Ansible Facts
      ansible.builtin.set_fact:
        ansible_app_ip: 192.168.100.90
        mysql_default_port: 3306/tcp

    - name: Import Vault Varaibles from secrets
      ansible.builtin.include_vars:
        file: secrets.yml


  roles:
    - role: users
    - role: firewall
      vars:
        - firewall_port: "{{ mysql_default_port }}"
        - firewall_state: enabled
        - permanent_state: true

    - role: database
      vars:
        - db_priviliges: '*.*:ALL'
        - db_name: ansible_app


```
Run the playbook with: 
```[user@workstation ~]$ ansible-playbook run.yml --ask-become-pass```
This was a longer lab, and if you followed along you did good. The limits of what you can do with ansible is set by your imagination. This is just a dip of a toe. Now we have deployed some fun stuff, 
got to know variables, ansible facts and much more. 

Doing all the tasks above will take very long time to do manually by cli, specially if you need to manually deploy the VM's also. With ansible you write the roles ones. And you and others can reuse them as many times you want, build on them etc. Ansible is not just for deployments but also for fixing errors, security fixes, scans, network and system configurations etc etc etc. <br>
So timing the playbook above and do all the same manuelly, i think the playbook would win by far with just some seconds to hours on the manual job. 

Well, what if i told you. We can do all we have done in all the labs even faster. Much faster and we dont need to think about VM resources OS type or version. 

In Lab 4 we will do everything we have learned uptil now and containerize it. 


### Self Study:
* Repeat the tasks above. 
* Create your own facts with ansible_set_fact
* Create a role to install mysql and use variables. Use ansible.include.role to unclude that role into the database role.
* Include the motd role with a fitting message for the database server
* Include the apache role + Add more ansible facts to the website template.
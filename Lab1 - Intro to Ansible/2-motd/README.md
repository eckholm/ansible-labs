# Message of the day
Message of the day is a fun and nice way to welcome someone that logs in to a host. 

## Content
Message of the day can be used to print a welcome message when someone is logging on to the server or to show messages and stats when logging in thru SSH. 
In this section we will look at creating files with Ansible and using **jinja templates**. We will use the filter module we did in ***1-ansible_debug*** task, to gather dates and hostname of the server to use in our message of the day. 
What we will introduce in this task is the "become" module. For us to move a file into the /etc directory we need to use Sudo / Administrator. For this we will use the "become" module and set this to true. For best practice we will do this for task spesific actions. To run this playbook we need to provide a password or a password file to the playbook. To make this easy we will run the play that need sudo password with the **'--ask-become-pass'** option. 

### Jinja templates: 
Ansible uses Jinja2 templating to enable dynamic expressions and access to variables and facts. You can use templating with the template module. For example, you can create a template for a configuration file, then deploy that configuration file to multiple environments and supply the correct data (IP address, hostname, version) for each environment. You can also use templating in playbooks directly, by templating task names and more. You can use all the standard filters and tests included in Jinja2. Ansible includes additional specialized filters for selecting and transforming data, tests for evaluating template expressions, and Lookup plugins for retrieving data from external sources such as files, APIs, and databases for use in templating.

## Tasks:

### 1. Create a template in the directory ./templates/motd1.j2 that we will use as a template for our new motd message
```
This is the system: {{ ansible_facts['hostname'] }}.
Today's date is: {{ ansible_facts['date_time']['date'] }}.
```
- Conclusion file: motd1.j2

### 2. Create a playbook to use this template to create a new motd file
```
- name: Ansible motd and templates
  hosts: localhost
  connection: local
  gather_facts: false

  tasks:
  - name: Ansible Fact filter
    ansible.builtin.setup:
      filter: 
        - 'ansible_hostname'
        - 'ansible_date_time'

  - name: Create a new motd message file
    become: true
    ansible.builtin.template:
      src: ./templates/motd1.j2
      dest: /etc/motd
      owner: root
      group: root
      mode: 0444

```
### 3. Create an inventory file. 
For our jinja template to work we need to point it to a group of hosts. Therefore we need an inventory file since we can not point it to something we prompt like localhost. 
Create a file called: inventory
```
[localhost]
localhost
```
Note: If you have changed the hostname of the server, the hostname goes in under the group name [localhost].
To run: 
``` [user@workstation ~]$ ansible-playbook -i inventory motd1.yml --ask-become-pass ```
- Conclusion playbook: motd1.yml


### Info: 
In the task above Ansible will fill in the variables created in the .j2 file. 


## Self Study: 
Create your own motd files using both tasks with filtering and your own text. 

## When finnished:
Run the cleanup script:
``` [user@workstation ~]$ sudo bash cleanup.sh```

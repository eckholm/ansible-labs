# Roles
Ansible roles let you automaticly load related variables, tasks, handlers and other Ansible artifacts in a play based on a file structure. This lets you reuse one or more role to many playbooks without having to re-write them. 
This makes roles easy to share with others also. 


## Content
In the section we will learn to create reusable roles and Ansible configuration files. While it seems alot, we have allready done this but using default values in Ansible.  


## Tasks: 

### 1. We will now reorder our file structure and start to organise our Ansible project into roles. First we will create our ansible.cfg (configuration file). 

Content of our **ansible.cfg** file:
```
[defaults]
roles = ./roles

[privilege_escalation]
become = false
```

### 2. Create the directories and files:
./roles/
./inventory

Your ansible structure should now look like: 
ansible.cfg 
roles/
inventory

### 3. Create roles with the ansible galaxy command
- ```[user@workstation ~]$ ansible-galaxy init roles/filter_facts```
- ```[user@workstation ~]$ ansible-galaxy init roles/motd``` 

Run command: 

```[user@workstation ~]$ tree roles/``` 

You will now see that our commands have created alot of directories. Fear not, here i will explain them: 

```

  roles/
      motd/                 # this hierarchy represents a "role"
          tasks/            #
              main.yml      #  <-- The tast file
          handlers/         #  
              main.yml      #  <-- Handler files, that can be called to action by the task role
          templates/        #  <-- files for use with the template resource
              motd.j2       #  <------- Out templates
          files/            #
              bar.txt       #  <-- files for use with the copy resource
              foo.sh        #  <-- script files for use with the script resource
          vars/             #
              main.yml      #  <-- Variables associated with this role
          defaults/         #
              main.yml      #  <-- Default lower priority variables for this role. URl's etc
          meta/             #
              main.yml      #  <-- role dependencies
          library/          # roles can also include custom modules, like if you write your own

```

### 4. Create the motd role task

Open the task file in the role **./roles/motd/tasks/main.yml** 
Add only the task from the motd task to this file after the ```---```

Create a directory called templates if it was not created by the ansible-galaxy command. (it does not do that anymore). 
**./roles/motd/templates**
Move our **motd.j2** template to this folder. 

Follow the same workflow as with the motd task with the filter_facts role. 

Documenting our work and autlmation jobs is important. Open the **~/README.md** and **~/meta/main.yml** in the conclusion directory to see how I did it this time. 

In our project directory, create a playbook to use the role. 
**~./ansible_roles.yml**

```
- name: Ansible Roles
  hosts: localhost
  connection: local
  gather_facts: false


  roles:
    - filter_facts
    - motd

```
```[user@workstation ~]$ ansible-playbook -i inventory ansible_roles.yml --ask-become-pass```
When we run this playbook, we can see that it did what it was supposed to do. It filtered facts and created a new motd message for us. 
While this is good and well, the playbook and roles are not very reusable and can only be used for this exact job. Lets change that so we in the playbook can point to whatever .j2 template we choose. 

### 5. Make our roles reusable

Lets change our filter to a list and loop so we can choose what to filter for: 
```
---
- name: Ansible Fact filter
  ansible.builtin.setup:
    filter: 
      - "{{ item }}"
  loop:
    - "{{ facts_items }}"

```
The item variables points us to a loop that also has a variable in its place. I know, inception right! It will make sense soon. 

Lets also change the task that uses our template, to be more reuseable. 
```
---
- name: Create a new motd message file with a j2 template
  ansible.builtin.template:
    src: "{{ motd_template }}"
    dest: /etc/motd
    owner: root
    group: root
    mode: 0444

```
As we can see above, we have removed the name of the .j2 template and put a variable in its place. Lets rap this up with our playbook.

```
- name: Ansible Roles and Variables
  hosts: localhost
  connection: local
  gather_facts: true


  roles:
  
    - role: filter_facts
      vars:
        facts_items:
        - 'ansible_hostname'
        - 'ansible_date_time'

    - role: motd
      become: true
      vars:
        motd_template: motd.j2

```

In the playbook above we have created a list of variables for the **filter_facts** role and provided the motd role with a template of our choosing. 
Why do all this? We want to reuse our roles as much as possible. This regards to all roles created. 

- Conclusion files: 1 / 2.

## Self Study
Create multiple jinja templates, and change the filters on them (if you choose to) and test.
Remember to have some extra eyes following all the variables. If you change the list of variables for the filter_facts, you also need to change them in the filter. 
Go beyond:
Remove the ansible.builtin.shell module in the post task and create a post_tasks: with one or more ansible modoules to do the same job. 


Tip: If the filtering is to much to handle, remove the filter role and just run the motd role. You can use ansible facts as you did in 1-ansible_debug and 2-motd without filter. 

# Ansible Debug and Fact gathering

Before we start to talk about **ansible debug**, we need to understand how Ansible is gathering facts about a host. 

## Ansible Facts
Ansible facts are data related to your remote systems, including operating systems, IP addresses, attached filesystems, and more. You can access this data in the ansible_facts variable. 
This is facts that Ansible can use as variables or use to getting to "know the system". 

You can test this on the localhost by running: 
```
[user@workstation ~]$ ansible localhost -m ansible.builtin.setup
```

In the output you will see all the facts Ansible can retrieve from the host you are pointing this command to. In this example on the host (lab). 
For exmaple: 

``` 
{
    "ansible_all_ipv4_addresses": [
        "192.168.20.42"
    ],
    "ansible_all_ipv6_addresses": [
    ]
},
    "ansible_distribution": "CentOS",
    "ansible_distribution_file_parsed": true,
    "ansible_distribution_file_path": "/etc/redhat-release",
    "ansible_distribution_file_variety": "RedHat",
    "ansible_distribution_major_version": "7",
    "ansible_distribution_release": "Core",
    "ansible_distribution_version": "7.5.1804",
    "ansible_dns": {
        "nameservers": [
            "127.0.0.1"
        ]
    },
```
Here we can see ansible has printed your systems IP address. All output can be used as a variable in Ansible to run a job. For example if you want to print out a IP address of your system, OS version, etc. 
You can also use this output to create filters in Ansible. 

Example: 
You want to patch only Ubuntu clients but not Centos systems. You can then run Ansible towards a large inventory with a filter like this: 
``` 
"{{ ansible_distrubution == 'Ubuntu' }}"
```

In most cases we do not need all these facts stored and it can slow down our playbook. We want our playbook to be fast, so therefore instead of using the "**gather_facts: true**" module we can create a task to only gather facts we need. 

In this case we only want the ip address of a host. 

Example: 
```
- name: Filter and return IPv4 only
  ansible.builtin.setup:
    gather_subset:
      - 'ansible_default_ipv4'
```



## Ansible debug
Facts is also great for using in the debug module, where you can print messages with spesific filters. 
Lets say we are running Ansible towards a host with the FQDN name (DNS: webservera.prod.local) and we want to know the ip address of that server. 
We can then run a playbook with the filter of: 
```
ansible_all_ipv4_addresses
```

Lets try that in a playbook. 


## Tasks: 

### 1. Create a playbook to print out all ansible facts from the host. Run the playbook.

```
- name: Ansible Debug and Facts
  hosts: localhost
  connection: local
  gather_facts: true


  tasks:
  - name: Ansible Debug Message
    ansible.builtin.debug:
      var: ansible_facts

```
Run the playbook: <br>
``` [user@workstation ~]$ ansible-playbook ansible_debug.yml -i localhost ``` <br>
You see we put the -i localhost in the prompt. This is to point the playbook towards an inventory of multiple hosts in an inventory file or in our case one host,- localhost.

- Conclusion playbook: ansible_debug.yml

### 2. Create a playbook to filter the output to only what we want and need. In this case the ip data. Run the playbook.
```
- name: Ansible Debug and Facts
  hosts: localhost
  connection: local
  gather_facts: false


  tasks:
  - name: Ansible Fact filter with only ipv4 address
    ansible.builtin.setup:
      filter: 
        - 'ansible_default_ipv4'

  - name: Create a variable of our facts we filtered
    ansible.builtin.debug:
      var: ansible_facts
    
  - name: Print only the IP address from our fact with another filter after the variable we created
    ansible.builtin.debug:
      msg: "{{ ansible_facts.default_ipv4.address }}"

```
Run the playbook: <br>
``` [user@workstation ~]$ ansible-playbook ansible_facts_filter.yml -i localhost ``` <br>
- Conclusion playbook: ansible_facts_filter.yml


### Info: 
Variables are shown as: "{{ some_variable }}"


## Self Study
Take a note on all the output from the complete set of data from "Ansible Gather Facts" and create filters as shown in task 2. Go beyond and see if you can create a file with the info you want. 
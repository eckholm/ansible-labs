# User Administration
## Manual way vs Ansible
In this lab we are going to look at how we can quickly create users and groups with Ansible. 
<i>Note: This lab doesnt go thru best pracices regarding passwords for users using vault. You will learn more about vault in Lab3.</i>

## Tasks:

### 1. Manual way

First we will create a user group and add some users to that group + create their home directorys the manual way thru CLI. <br>
What we want to do is to create a user group called 'trainees' and create 5 users to this group. We also want to create a second group for admin and populate it with one admin user <br>
Now to do this, follow these steps:

```[user@workstation ~]$ groupadd -g 5000 trainees``` <br>
```[user@workstation ~]$ groupadd -g 7000 cli_admins``` <br>
<i>'-g 5000' represent the group id. All ids under 1000 is set for system accounts/groups.</i>

Verify that the user group is added. <br>
```[user@workstation ~]$ tail /etc/group``` <br>
You should then see your group in the returns. 

Now lets create our users: <br>

```[user@workstation ~]$ useradd user1```               <---Create User <br>
```[user@workstation ~]$ passwd user1```                <---Create password for the user<br>
```[user@workstation ~]$ usermod -aG tranees user1```   <---Adds user to the trainee group<br>

Do this for all the 5 users. User1, User2 etc. 

Now we want to create a new admin users access:<br>
```[user@workstation ~]$ useradd useradmin1``` <br>
```[user@workstation ~]$ passwd useradmin1``` <br>
```[user@workstation ~]$ usermod -aG cli_admins useradmin1``` <br>

Now confirm that the user is added to the 'wheel' group by running: <br>
```[user@workstation ~]$ cat /etc/group | grep cli_admins``` <br>

That sucked didnt it? Lets do the same with Ansible and using roles. 

### 2. Create files and directories we need:
./inventory <br>
./roles/ <br>
./ansible.cfg <br>

```
[defaults]
collections = ./collections/
roles = ./roles/

[privilege_escalation]
become = true
become_method = sudo
become_user = root
become_ask_pass = true
```
./ansible.cfg

Create the inventory file:
./inventory
```
[localhost]
localhost
```
Create our role:<br>
```[user@workstation ~]$ ansible-galaxy init roles/create_users ```

Earlier we have only used the 'tasks/main.yml' playbook. Lets start to use the 'default/main.yml' also but before that, we want to create a file to host all of our userdata of the users we want to create. We then want to loop thru this file with Ansible to create the users we want. Create the file ./groups.yml in the project root directory.

```
- name: trainees
  members:
    - Ansible-User1
    - Ansible-User2
    - Ansible-User3
    - Ansible-User4
    - Ansible-User5
- name: ansible_admins
  members:
    - Ansible-useradmin1

```
<i>Since we allready create user1, user2 etc from the task 1, we now just add Ansible-User infront of the username, to know which was created by Ansible.</i>
./groups.yml


Now lets create our tasks/main.yml in the role with a loop. We will set the 'user_groups' variable in the main playbook.
./roles/create_users/tasks/main.yml
```
---
- name: Create trainees user group
  ansible.builtin.group:
    name: "{{ item.name }}"
    state: present
  loop: "{{ user_groups }}"

- name: Create users with defaults/main.yml default data
  ansible.builtin.user:
    name: "{{ item }}"
    password: "{{ password }}"
    shell: "{{ default_shell }}"
    state: "{{ default_state }}"
  loop: "{{ users_loop }}"

- name: Add users to the correct groups
  ansible.builtin.user:
    name: '{{ item.1 }}'
    groups: "{{ item.0.name }}"
    append: true
  loop: "{{ create_users_loop }}"
```
Now, we want all our users to have 'bash' as the default shell and also a default number of warning days before password of the user expires.

Add content to: ./roles/create_users/defaults/main.yml
```
---
default_shell: /bin/bash
default_state: present
```
Now that we have created our roles with tasks and defaults data, we can start to create our main playbook. Before we do this, we need some passwords for the users. <br> Ansible will not take "clear text" passwords and only hashed password strings. Find the 'users.txt' file under help_files and copy/move it to the project root directory. 

In this playbook we want to add a "Pre task". This is a task that will run before our roles. We use this to f.ex setting facts in ansible. Remember Task1 and 2? <br>
Yes, just like Fox News, we can set our own facts in Ansible... :D <br>
Lets implement our pre_tasks:<br>
<i>For simplicity and to not become overloaded we will use the same password hash on all users. DO NOT DO THIS IN PRODUCTION! password is: password</i>

create_users.yml
```
---
- name: Create users and groups
  hosts: localhost
  connection: local
  gather_facts: false


  pre_tasks:
    - name: Use the 'lookup' plugin to load our groups.yml file for our role
      ansible.builtin.set_fact:
        user_groups: "{{ lookup('file', 'groups.yml') | from_yaml }}"

  roles:
    - role: create_users
      vars:
        users_loop: "{{ query('lines','cat users.txt') }}"
        password: '$2y$12$uMhrWyiihcB7TiYK7mi9A.52T08otgLhmd5vpQDcpHLd/d86eQ5zC'
        create_users_loop: "{{ user_groups | subelements('members', 'skip_missing=true') }}"

```

Now run the playbook. 
Run the commands from task 1 to see if the users and groups are created. or run:  ```[user@workstation ~]$ id Ansible-User1```   <br>
Same with groups: ```[user@workstation ~]$ groups trainees``` <br>
You can login as one of the users with: ```[user@workstation ~]$ su Ansible-user1``` <br>
Then verify if you are logged in as the correct user:  ```[user@workstation ~]$ whoami```

If you need to delete a user, run: ```[user@workstation ~]$ userdel --remove --selinux-user 'username'```. This command will delete the user and their home dir.



## Self Study:
Split our role into two roles. Create one role for the creation of groups and one role for the creation of users. <br>

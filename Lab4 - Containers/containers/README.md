# Containerisation of what we have learned

### In this lab we will learn:
Containerize all what we have learned uptil now when deploying applications on our server. (Apache web app and mysql database) <br>
Containers is a small base OS like linux, with very few services and applications installed on it. / Lightweight. <br>
<br>
NB: this lab doesnt follow best practices, but is just to learn a little about containers and ansible together with containers. <br>
Each Containerfile/Dockerfile should be stored in a git repository with its configuration files etc. <br>
<br>
In this lab we will create our roles as before but use variables in all of them. We will set some facts in a **pre_tasks** etc. 

# Tasks:
### 1. Lets first create all the files we need: ansible.cfg, inventory, roles, collections etc. 
~./ansible.cfg
```
[defaults]
collections     = ./collections
roles           = ./roles
inventories     = ./inventory

[privilege_escalation]
become = false
become_method = sudo
become_user = root
become_ask_pass = true

[galaxy]
url=https://galaxy.ansible.com
```
Roles: <br>
- firewall
- mysql_container
- web_container

~./collections/requirements.yml
```
---
collections:
- name: containers.podman
  version: 1.10.3

- name: ansible.posix
  version: 1.5.4
```
Install the collections with the command we learned from Lab3

### 2. Create our Dockerfile for the web container. 
We want to use Red Hats base apache img and then build on top of that. This is nice, because we dont need to build everything from scratch but use a allready <br>
built container image that has all default configs, and build ontop of that. <br>
Create a directory called 'files' in the root dir of you project folder. <br>
<br>
```[user@workstation ~]$mkdir files```<br>
<br>
Now, lets create our container file in this directory. <br>
<br>
<i>Dont include the comments (#), they are just for you</i> <br>
~./files/Dockerfile <br>
```
# Where to pull the container image from
# Uncomment what suits you
#FROM registry.redhat.io/rhel8/httpd-24         # If you have an Red Hat Account (Also remove this comment)
#FROM quay.io/fedora/httpd-24                   # If you dont have an Red Hat Account (Also remove this comment)

# Creates a new directory in the container image
CMD mkdir /var/www/html/fonts

# Copys our app data to the container image
COPY www/index.html /var/www/html/index.html
COPY www/fonts/* /var/www/html/fonts/

# Then run script uses standard ways to run the application
CMD run-httpd
```
Then Copy the help_files over to the 'files' directory. <br>

### 3. Create our roles <br>
~./roles/firewall/tasks/main.yml 
```
---
- name: Configure Firewall for web container
  ansible.posix.firewalld:
    port: "{{ firewall_port_web }}"
    permanent: "{{ permanent_state_web }}"
    state: "{{ firewall_state_web }}"

- name: Configure firewall for mysql container
  ansible.posix.firewalld:
    port: "{{ firewall_port_mysql }}"
    permanent: "{{ permanent_state_mysql }}"
    state: "{{ firewall_state_mysql }}"


```
The mysql role: We will pull the default container from Red Hat and not reconfigure it (as you should in production!!)<br>
~./roles/mysql_container/tasks/main.yml <br>

```
---
- name: Deploy mysql container
  containers.podman.podman_container:
    name: "{{ mysql_container_name }}"
    image: "{{ mysql_container_registry }}"
    state: "{{ mysql_container_state }}"
    ports:
      - "{{ mysql_container_ports }}"

```
~./roles/web_container/tasks/main.yml <br>
```
---
- name: Create Container Image ontop of the redhat img
  containers.podman.podman_image:
    name: "{{ web_container_name }}"
    path: "{{ web_path_to_buildfile }}"
    state: "{{ web_container_img_state }}"

- name: Deploy the container image we built
  containers.podman.podman_container:
    name: "{{ web_container_name }}"
    image: "{{ web_container_name }}"
    state: "{{ web_container_state }}"
    ports:
      - "{{ web_container_ports }}"

```

<br>
<br>
<br>
Now that we have created our roles we can create our main playbook.<br>
~./deploy.yml

```
---
- name: Containerize
  hosts: localhost
  connection: local
  gather_facts: false


  pre_tasks:
    - name: Setting ansible facts
      ansible.builtin.set_fact:
        web_container_port_out: 8081
        web_container_default_port: 8080
        mysql_container_ports: 3306
        default_port_protocol: tcp

  roles:
    - role: firewall
      become: true
      vars:
      # Web Config
        - firewall_port_web: "{{ web_container_port_out }}/{{ default_port_protocol }}"
        - permanent_state_web: true
        - firewall_state_web: enabled
      # Mysql Config
        - firewall_port_mysql: "{{ mysql_container_ports }}/{{ default_port_protocol }}"
        - permanent_state_mysql: true
        - firewall_state_mysql: enabled
    
    - role: mysql_container
      vars:
        - mysql_container_name: RH-mysql
        # Uncomment what suits you
        # - mysql_container_registry: registry.redhat.io/rhel8/mysql-80  #If you have an Red Hat account
        # - mysql_container_registry: quay.io/fedora/mysql-80            #If you dont have a Red Hat account
        - mysql_container_state: started
        - mysql_container_ports: "{{ mysql_container_ports }}:{{ mysql_container_ports }}"

    - role: web_container
      vars:
      # Container img build
        - web_container_name: rhelansweb
        - web_path_to_buildfile: ./files
        - web_container_img_state: present
      # Container deployment
        - web_container_state: started
        - web_container_ports: "{{ web_container_port_out }}:{{ web_container_default_port }}"
  

```

<br>
<br>

#### Great job! You have now containerized everything. You should now have two podman containers running and you can check that by running the commands:<br>

```[user@workstation ~]$ podman ps```<br>

<br>
You should now see a list of containers you have a deployed. <br>
You can also open your web browser to list visit the webpage at url: http://localhost:8081 <br>

## Self Study:
- Rewrite the firewall role to use less roles, and only one set of tasks. (Hint: Use loops)
- We want to hide the ports in an Vault encrypted file. Use vault to encrypt a variable file and use that file as variables for all roles.
- Add more roles: <br>
-- 'motd' that fits the server role <br>
-- more roles? <br>
- Remove all variables from the playbook and use variable files instead.
- Read up on 'using group vars'

## Go beyond
### The big one. - Save time! 

#### Lets do everything without ansible.
<i>You dont need to do all tasks, but do what you can and use google and guides online. This is for you to understand why we want to automate </i>

Reinstall the RHEL VM to a clean installation and do not pull this git repository. <br>
Do everything manually! **Google it if you need to** <br>

##### 1. Visual Studio Code

Install Visual Studio code

##### 2. motd

Create a fitting motd (/etc/motd)

##### 3. apache

Install apache and deploy the HTML from Lab2.  <br>
Edit the file and remove all j2 variables: ex: ```{{ hostvars[item].ansible_distribution }}``` and add the correct data in its place. <br>
Configure apache to use its file as website.

##### 4. mysql

Install and deploy mysql with the same settings as we did with ansible in Lab3. <br>

##### 5. Podman Container

Deploy an apache container (httpd) with the website from Lab4 using podman.
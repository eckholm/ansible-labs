# Ansible and contunius delivery CI/CD 

### In this lab we will look at:
- Templating html sites
- Ansible collections
- Installing Packages
- Configuring firewalls
- Deploy a website with Ansible with system info

We will create a contiues deployment of a website that gives us our system data. And again, we will make our roles as reusable as possible.

## Ansible Collections:
A ansible collection is a collection with many modules for ansible. F.ex ansible.windows. Here you have many many modules for administrating Windows systems. You can read more on the official ansible.com/collections site. 

When we have called our modules in earlier tasks we have used the module prefix: **ansible.builtin.module_name** This is modules that is builtin to the ***ansible core***. Collections have the last years been foked out of ansible to make ansible 
lighter to run and also the maintaining of ansible easier for developers. Now they dont have to maintain ansible and all the collections, since collections can be built by the community and system vendors like Cisco, VmWare, Microsoft etc. We can therefore install collections from ansible galaxy (galaxy.ansible.com) in two ways. 
The command: ```[user@workstation ~]$ ansible-galaxy install namespace.collection``` or with an requirement file within our collections directory. (you will see this in your first task here). 

In the case of these labs, the collections is allready in the ***~./help_files/***. You just need to move it and install it. (we will do that shortly)
We will now also introduce the collections path in our configuration file. 

# Tasks

### 1. First, lets create our project folder, directory structure and our ansible.cfg file in the root of our project folder.

ansible.cfg
```
[defaults]
collections = ./collections/
roles = ./roles/

[privilege_escalation]
become = false

# This is allready installed on your system. This is for when the collections is not installed. 
# [galaxy]
# url=https://galaxy.ansible.com 

```
Lets create two more directories in the root of the project:<br>
~./collections<br>
~./roles<br>

Lets create the inventory file:
```
[localhost]
localhost
```


You direcotry should now look like: 
```
ansible.cfg
collections/
roles/
inventory
```

Move the tar.gz file from the help_files/collections to the project root directory and install the collection with command: 
```[user@workstation ~]$ ansible-galaxy collection install ansible-posix-1.5.4.tar.gz -p ./collections ```  

### 2. Now we need to create our roles
```
- [user@workstation ~]$ ansible-galaxy init roles/website     # <--- This role will create our website with host system report based on a template
- [user@workstation ~]$ ansible-galaxy init roles/install     # <--- This role will install the packages we need. In this case apache
- [user@workstation ~]$ ansible-galaxy init roles/firewall    # <--- This role we will configure our firewall
- [user@workstation ~]$ ansible-galaxy init roles/deploy      # <--- This role will deploy the website and configure the webservice
```

All tasks under is in the **~./*/tasks/main.yml** directory
### 3. Website role
We want Ansible to gather facts about the host and pass them on to a j2 template, and store the website temp in the root of our project folder (you can also move it directly to the apache folder). <br>
Lets create our website task <br>
Move the .j2 template from help files to the template directory under the website role.
```
--- 
- name: Create web_report from template
  ansible.builtin.template:
    src: templates/web_report.html.j2
    dest: "{{ website_destination }}"
  run_once: true

```
Customise the meta and readme files to your needs. <br>
You delete unused directories in the role if you want to. It can be easier while it is cleaner.

### 4. Install role
It is not much we need to install on the system other then a webservice. In this case apache but with the use of variable for reusability. We will also use become on this task since it needs sudo access. <br>
(If you do not have internett access. This package will allready be installed. If that is the case, still do the task. Ansible will just skip it.)
```
---
- name: Install package
  become: true
  ansible.builtin.package:
    name: "{{ item }}"
    state: present
  with_items: "{{ install_packages }}"

```

### 5. Configure firewall
This task requere us to use the module ansible.posix.firewalld which is part of the ansible.posix collection. <br>
For our webserver to be able to talk to us, we need to allow the port in our firewall. Lets open the web port: 80/tcp.  <br>
It is two ways to open port on a linux host. Open service with default port or open port. 

We do not need to reload the firewalld afterwords since the module will do that for us when using the prefix: **```state: enabled```**

```
---
- name: Open firewall for webservice
  become: true
  ansible.posix.firewalld:
    service: http
    permanent: true
    state: enabled

```

### 6. Lets deploy our website with the fact report from our website template + bonus - smoketest
Now we are going to do something fun. We will not run the role: website as a role in the main playbook, but run it as a role within a task in a role! Wait what? <br>
Yes, in roles and tasks we can include other roles, and we will try that here. 

As a bonus we will include a smoketest as a handler and a default varaible. A handler is a ansible job that can be called on after the main task is ran, or during. <br>
In this case we will use a handler to check if the site is running as we want. We can call a handler with the prefix: **```notify: handlername```** <br>
We will also restart apache service to serve our website and not the default apache website.

The default variable will only host the default directory of our apache website directory

~/task/main.yml
```
---
- name: Copy the website to the web service
  ansible.builtin.include_role:
    name: website
  vars:
    website_destination: "{{ apache_dest }}"

- name: Restart apache service
  ansible.builtin.service:
    name: httpd
    state: restarted
  notify: Smoketest

```
~/handlers/main.yml
```
---
- name: Smoketest
  ansible.builtin.uri:
    url: http://localhost
    return_content: true
  register: this
  failed_when: "'This is awsome!' not in this.content"

```
~/defaults.main.yml
```
apache_dest: /var/www/html/index.html
```

Now we have created all of our roles. It is alot to grasp, but we got there. <br>
Now, finally lets create the main playbook to run all our roles with its variables, templates and all its beauty. 

The the root project folder create the run.yml playbook <br>
~./run.yml
```
- name: Deploy our awsome Website
  hosts: all
  connection: local
  gather_facts: true


  roles:
  # Install apache
    - role: install
      vars: 
        install_packages: httpd
  # Configure firewall
    - role: firewall
  # Deploy the website + smoketest
    - role: deploy
      become: true

```

You can now visit the website your self at: http://localhost

## Conclusion: 
What have we done now. <br>
We have used ansible roles, templates, different variables and many different modules from multiple collections to do our work. <br>
Doing all of this manually would take some time. Specially if you need to code in all values of the facts we found with ansible. 

When running this play you did this in under a minute. Now upscale this to many many servers, services, deployments etc and you should now start to grasp the 
greatness of Ansible automation. 

We are not done here. In the next task Lab, we will go deeper with best practice and more variable use. And later on. Containerisation. 

## Self Study
- Do the task multiple times. 
- Change the firewall role to open port instead of service (use official documenation)
- Move the smoketest handler to a post_task in the main playbook. 
- Use variables instead of: 
    - firewall service, states and other places. 
- Add url of the homepage to the motd in a motd role. 
Deploy
=========

This role will deploy our website to the web service and restart the apache/httpd service

Requirements
------------

#

Role Variables
--------------

#

Dependencies
------------

- website

Example Playbook
----------------


    - hosts: servers
      roles:
         - deploy

License
-------


Author Information
------------------

Dan Eckholm

Install
=========

Install packages on system

Requirements
------------

#

Role Variables
--------------

#

Dependencies
------------


Example Playbook
----------------


    - hosts: servers
      roles:
         - install

License
-------


Author Information
------------------

Dan Eckholm

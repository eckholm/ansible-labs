Website
=========

Creates our website based on a jinja2 template

Requirements
------------

#

Role Variables
--------------

#

Dependencies
------------


Example Playbook
----------------


    - hosts: servers
      roles:
         - website

License
-------


Author Information
------------------

Dan Eckholm

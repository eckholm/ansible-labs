Users
=========

Create Users

Requirements
------------

Sudo access to system

Role Variables
--------------

    name: "{{ username }}"
    password: "{{ password }}"

Dependencies
------------

Ansible Vault secret file

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
        vars_files:
          - secrets.yml

License
-------

BSD

Author Information
------------------

Dan Eckholm

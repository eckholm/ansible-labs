Firewall
=========

Configure Firwall on system

Requirements
------------

Ansible.posix collection

Role Variables
--------------

    source: "{{ source_ip_address }}"
    port: "{{ firewall_port }}"
    permanent: "{{ permanent_state }}"
    state: "{{ firewall_state }}"

Dependencies
------------

#

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
        vars:
          - firewall_ip_address: "{{ ansible_app_ip }}"
          - firewall_port: "{{ mysql_default_port }}"
          - firewall_state: enabled
          - permanent_state: true

License
-------

BSD

Author Information
------------------

Dan Eckholm

Database
=========

Deploy and configure MYSQL Server + Client and database

Requirements
------------

- Sudo acceess to server
- Collection: mysql
- MYSQL-Server package
- MYSQL-Client Package

Role Variables
--------------

db_username: "{{ username }}"
db_password: "{{ password }}"

Dependencies
------------

secrets.yml vault secret file

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
        - role: database
        vars_files::
          - secrets.yml
        vars:
          - db_priviliges: '*.*:ALL'
          - db_name: ansible_app

License
-------

BSD

Author Information
------------------

Dan Eckholm

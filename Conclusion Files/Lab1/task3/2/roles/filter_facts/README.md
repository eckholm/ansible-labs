filter_facts
=========

Filtering ansible facts

Requirements
------------
#



Example Playbook
----------------


    - name: Creating Roles
      hosts: localhost
      gather_facts: false

      roles:
         - filter_facts

License
-------

BSD

Author Information
------------------

Dan Eckholm

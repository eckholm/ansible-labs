motd - Message of the day
=========

Creates new 'message of the day' message

Requirements
------------

Sudo access to the host


Example Playbook
----------------



    - name: Creating Roles
      hosts: localhost
      gather_facts: false

      roles:
         - motd

License
-------

BSD

Author Information
------------------

Dan Eckholm


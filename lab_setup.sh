#! /bin/sh
# Start Script to check if the VM is ready to use for the Ansible Labs.
# This script needs to be ran on the server which you want to run Ansible from.
# Script needs to be ran with root priviligues
echo
echo
echo "Checking VM and installing packages"
echo
####
####
echo "Installing Ansible-core..."
dnf install -y ansible-core
####
####
echo "Installing Python3-pip..."
dnf install -y python3-pip
####
echo "Installing Ansible-Lint..."
pip3 install ansible-lint
echo
####
echo "Enabling cockpit and configuring firewall..."
systemctl enable --now cockpit.socket
firewall-cmd --add-service=cockpit --permanent
firewall-cmd --reload
####
echo "Starting and enabling podman service..."
systemctl start podman
systemctl enable podman
####
echo "Installing vsCode..."
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'
dnf check-update
dnf install -y code
####
echo "###############################################################"
echo
echo "You lab VM is now ready!"
echo "Cockpit available at url: https://localhost:9090"
echo
echo "To open Visual studio code, simply just type 'code' in the Terminal."
echo "Printing ansible versions..."
echo
echo "###############################################################"

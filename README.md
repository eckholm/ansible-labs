# Ansible Labs

## Description
Ansible labs is a git repository with labs for learning Ansible. The labs is for everyone from new users and for more advanced users who wants to learn more about Ansible. 
Documentation and conclusion for each lab will follow in their folders. The labs also includes containerization.

The lab can be ran on a single VM or against multiple VMs all depending on the inventory file. When running against a localhost (single vm) you will need to add: 'connection: local' in the playbook as seen in the conclusion files. 
The lab will go over both Ansible and containerization.

There is cleanup scripts that has to be ran as sudo in each lab and subfolder. While this can be used it is recommended to take a snapshot of the vm before the larger labs from Lab3 and out or to use a clean VM (snapshot from clean install)


## Getting started
- Clone this repository to a clean RHEL9 VM and read the documentation of each lab with its tasks.
- Run the lab_setup.sh with sudo privileges to install all needed packages and tools.
- The lab_setup.sh will also install and enable cockpit service which is a web interface to administrate the RHEL VM. 
- You can enter cockpit by visiting the URL: https://localhost:9090 
- Read more here: 
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/9/html-single/managing_systems_using_the_rhel_9_web_console/index (You may need an free Red Hat Account)
- If you dont have a RHEL9 VM, you can get free Red ht resrouces when signing up for the development program at developers.redhat.com


## Integrate with your tools
Ansible can be used to automate simple to advanced tasks and can be integrated with tools you allready have today. Read more in the official Ansible documentation or contact an Ansible admin. 

## Other inforamtion + Disclosure
Many of the tasks in these labs are not ment to be used in production, while you can use it for guidens, do not use it as a "Best Practice". <br>
These labs are built for simplicity and for easy learning and easy to update and build for me. <br>
Allways use a .gitignore file to add your secrets like passwords, special config files etc so they dont get pushed to a public git.<br>
You can run ```$ ansible-playbook -run.yml --check``` or ***--diff*** to test the playbook before you run it. <br>
Also be used to use ansible-lint to check your playbooks.<br>


## Contributing
Want to contribute with this lab? Please contact author: Dan Eckholm or fork the project.

## Authors and acknowledgment
Author: Dan Eckholm

## License
Free

## Project status
Allways in development :-)
